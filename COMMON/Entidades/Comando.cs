﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Entidades
{
    public class Comando:BaseDTO
    {
        public string IdDispositivo { get; set; }
        public string ComandoTexto { get; set; }
        public string Origen { get; set; }
        public override string ToString()
        {
            return $"{FechaHora}:[{Origen}] {ComandoTexto}";
        }
    }
}
