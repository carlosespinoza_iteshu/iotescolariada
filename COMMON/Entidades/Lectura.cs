﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Entidades
{
    public class Lectura:BaseDTO
    {
        public string IdDispositivo { get; set; }
        public float Temperatura { get; set; }
        public float Humedad { get; set; }
        public override string ToString()
        {
            return $"Temp:{Temperatura} Hum:{Humedad}";
        }

    }
}
