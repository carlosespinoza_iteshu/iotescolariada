﻿using COMMON.Entidades;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace COMMON.Interfaces
{
    public interface IGenericRepository<T> where T:BaseDTO
    {
        T Create(T entidad);
        IEnumerable<T> Read { get; }
        T Update(T entidad);
        bool Delete(string id);
        T SearchById(string id);
        IEnumerable<T> Query(Expression<Func<T, bool>> predicado);

    }
}
