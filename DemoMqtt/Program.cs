﻿using System;

namespace DemoMqtt
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Demo MQTT");
            DALMqtt.MqttService service = new DALMqtt.MqttService("broker.hivemq.com", 1883, "ProfeCarlos");
            if (service.Conectado)
            {
                service.MensajeRecibido += Service_MensajeRecibido;
                service.Suscribir("5c7d8b53f8c8a11cd459e675");
            }
            Console.WriteLine("Escriba el mensaje a enviar (Para salir de enter):");
            string mensaje = "";
            do
            {
                mensaje = Console.ReadLine();
                service.Publicar("iot", mensaje);
            } while (mensaje!="");
        }

        private static void Service_MensajeRecibido(object sender, string e)
        {
            Console.WriteLine(e);
        }
    }
}
