﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PlataformaWeb.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class LecturaController : GenericApiController<Lectura>
    {
        public LecturaController(IGenericRepository<Lectura> repo) : base(repo)
        {
        }
    }
}