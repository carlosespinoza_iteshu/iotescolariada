﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Newtonsoft.Json;

namespace PlataformaWeb.Api
{
    public abstract class GenericApiController<T> : ControllerBase where T : BaseDTO
    {
        private IGenericRepository<T> repository;
        public GenericApiController(IGenericRepository<T> repo)
        {
            repository = repo;
        }
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Insertar([FromBody] T entidad)
        {
            try
            {
                return Ok(repository.Create(entidad));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Buscar(string id)
        {
            try
            {
                var r = repository.SearchById(id);
                if (r != null)
                {
                    return Ok(r);
                }
                else
                {
                    return NotFound("Objeto no encontrado...");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [HttpPut("{id}")]
        public IActionResult Actualizar([FromBody]T entidad,string id)
        {
            try
            {
                var r = repository.SearchById(id);
                if (r != null)
                {
                    entidad.Id = r.Id;
                    T e = repository.Update(entidad);
                    if (e != null)
                        return Ok(e);
                    else
                        return BadRequest("No se pudo actualizar el elemento...");
                }
                else
                {
                    return NotFound("Objeto no encontrado...");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        public IActionResult Listar()
        {
            try
            {
                return Ok(repository.Read);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete("{id}")]
        public IActionResult Eliminar(string id)
        {
            try
            {
                var r = repository.SearchById(id);
                if (r != null)
                {
                    if (repository.Delete(r.Id))
                        return Ok("true");
                    else
                        return BadRequest("No se pudo eliminar el elemento...");
                }
                else
                {
                    return NotFound("Objeto no encontrado...");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}