﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using COMMON.Entidades;
using COMMON.Interfaces;
using DALMongoDB;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PlataformaWeb.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class InsertController : ControllerBase
    {
        IGenericRepository<Dispositivo> dispositivoRepository;
        IGenericRepository<Lectura> lecturaRepository;
        public InsertController()
        {
            dispositivoRepository = new GenericRepository<Dispositivo>();
            lecturaRepository = new GenericRepository<Lectura>();
        }
        public IActionResult Post(string idDisp,string pass,float temp,float hum)
        {
            try
            {
                Dispositivo d = dispositivoRepository.SearchById(idDisp);
                if (d != null)
                {
                    if (d.Password == pass)
                    {
                        Lectura l = new Lectura()
                        {
                            IdDispositivo = d.Id,
                            Humedad = hum,
                            Temperatura = temp
                        };
                        Lectura r = lecturaRepository.Create(l);
                        if (r != null)
                        {
                            return Ok(r);
                        }
                        else
                        {
                            return BadRequest("Error al ingresar la lectura");
                        }
                    }
                    else
                    {
                        return BadRequest("Contraseña incorrecta");
                    }
                }
                else
                {
                    return BadRequest("Dispositivo no encontrado");
                }
            }
            catch (Exception)
            {
                return BadRequest("Error en el servidor...");
            }
        }
    }
}