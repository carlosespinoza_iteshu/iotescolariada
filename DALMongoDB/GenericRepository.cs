﻿using COMMON.Entidades;
using COMMON.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Text;

namespace DALMongoDB
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        private MongoClient client;
        private IMongoDatabase db;
        public GenericRepository()
        {
            client = new MongoClient(new MongoUrl(@"mongodb://profecarlos:moonlight19@ds018308.mlab.com:18308/plataformaiot"));
            db = client.GetDatabase("plataformaiot");
        }
        private IMongoCollection<T> Collection()
        {
            return db.GetCollection<T>(typeof(T).Name);
        }
        public IEnumerable<T> Read => Collection().AsQueryable();

        public T Create(T entidad)
        {
            entidad.Id = ObjectId.GenerateNewId().ToString();
            entidad.FechaHora = DateTime.Now;
            try
            {
                Collection().InsertOne(entidad);
                return entidad;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                return Collection().DeleteOne(e => e.Id == id).DeletedCount == 1;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado)
        {
            return Collection().Find(predicado).ToEnumerable();
        }

        public T SearchById(string id)
        {
            return Collection().Find(e => e.Id == id).SingleOrDefault();
        }

        public T Update(T entidad)
        {
            entidad.FechaHora = DateTime.Now;
            try
            {
                //return entidad;
                return Collection().ReplaceOne(e=>e.Id==entidad.Id,entidad).ModifiedCount == 1 ? entidad : null;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
