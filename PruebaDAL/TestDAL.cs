﻿using System;
using System.Linq;
using COMMON.Entidades;
using COMMON.Interfaces;
using DALMongoDB;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PruebaDAL
{
    [TestClass]
    public class TestDAL
    {
        #region Dispositivo
        [TestMethod]
        public void TestCreateDispositivo()
        {
            Dispositivo d = new Dispositivo()
            {
                Nombre = "Dispositivo de Pruebas",
                Password = "eldesiempre"
            };
            IGenericRepository<Dispositivo> repository = new GenericRepository<Dispositivo>();
            int antes = repository.Read.Count();
            Dispositivo r = repository.Create(d);
            int despues = repository.Read.Count();
            Assert.IsNotNull(r);
            Assert.AreEqual(antes +1, despues, "No se agrego...");
            if (r != null)
            {
                repository.Delete(r.Id);
            }
        }

        [TestMethod]
        public void TestDispositivoUpdate()
        {
            Dispositivo d = new Dispositivo()
            {
                Nombre = "Dispositivo de Pruebas",
                Password = "eldesiempre"
            };
            IGenericRepository<Dispositivo> repository = new GenericRepository<Dispositivo>();
            Dispositivo r = repository.Create(d);
            r.Password = "Ya es otro";
            Dispositivo r2 = repository.Update(r);
            Assert.AreEqual(r.Password, r2.Password, "No se modifico");
            Assert.AreEqual(1, repository.Query(t => t.Password == "Ya es otro").Count());
            repository.Delete(r.Id);
        }
        #endregion
    }
}
