﻿using COMMON.Entidades;
using COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ConsultanteMovil
{
    public partial class MainPage : ContentPage
    {
        IGenericRepository<Lectura> repository;
        public MainPage()
        {
            InitializeComponent();
            pkrOrigen.Items.Add("MongoDB");
            pkrOrigen.Items.Add("API");
        }

        private void BtnObtener_Clicked(object sender, EventArgs e)
        {
            if (pkrOrigen.SelectedItem.ToString() == "MongoDB")
            {
                repository =new DALMongoDB.GenericRepository<Lectura>();
            }
            else
            {
                repository =new DALApi2.GenericRepository<Lectura>();
            }
            lstDatos.ItemsSource = null;
            lstDatos.ItemsSource = repository.Read.Where(l => l.IdDispositivo == entId.Text);
        }
    }
}
