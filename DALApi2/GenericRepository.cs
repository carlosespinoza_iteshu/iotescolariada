﻿using COMMON.Entidades;
using COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DALApi2
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        private RestService<T> ws;
        public GenericRepository()
        {
            ws = new RestService<T>("https://plataformaiotescolarizada.azurewebsites.net", "/api/" + typeof(T).Name + "/");

        }
        public IEnumerable<T> Read => ws.ObtenerDatos().Result;

        public T Create(T entidad)
        {
            return ws.Guardar(entidad).Result;
        }

        public bool Delete(string id)
        {
            return ws.Eliminar(id).Result;
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predicado)
        {
            return Read.Where(predicado.Compile()).AsQueryable();
        }

        public T SearchById(string id)
        {
            return ws.ObtenerUno(id).Result;
        }

        public T Update(T entidad)
        {
            return ws.Actualizar(entidad).Result;
        }
    }
}
