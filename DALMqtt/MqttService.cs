﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using OpenNETCF.MQTT;

namespace DALMqtt
{
    public class MqttService
    {
        MQTTClient mqtt;
        public bool Conectado { get; private set; }
        public event EventHandler<string> MensajeRecibido;
        public MqttService(string servidor, int puerto,  string cliente, string usuario="", string password="")
        {
            int intentos = 0;
            mqtt = new MQTTClient(servidor, puerto);
            if (usuario == "")
            {
                mqtt.Connect(cliente);
            }
            else
            {
                mqtt.Connect(cliente, usuario, password);
            }
            Conectado = mqtt.IsConnected;
            while (!mqtt.IsConnected)
            {
                Thread.Sleep(1000);
                Debug.Write(".");
                intentos++;
                if (intentos == 120)
                {
                    Debug.WriteLine("\nNo se pudo conectar...");
                    Conectado = false;
                    return;
                }
            }
            Conectado = mqtt.IsConnected;
            if (mqtt.IsConnected)
            {
                mqtt.MessageReceived += Mqtt_MessageReceived;
            }
        }
       

        public bool Suscribir(string tema)
        {
            if (Conectado)
            {
                try
                {
                    mqtt.Subscriptions.Add(tema, QoS.FireAndForget);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public bool Publicar(string tema, string mensaje)
        {
            if (Conectado)
            {
                try
                {
                    mqtt.Publish(tema, mensaje, QoS.FireAndForget, true);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private void Mqtt_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            MensajeRecibido(this, $"[{topic}]{System.Text.Encoding.UTF8.GetString(payload)}");
        }
    }
}
