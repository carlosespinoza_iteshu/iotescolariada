﻿using COMMON.Entidades;
using COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Consultante
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("De donde quieres traer los datos?[1: MongoDB], [2: WebApi]");
            int opcion = int.Parse(Console.ReadLine());
            IGenericRepository<Lectura> repositorio;
            if (opcion == 1)
            {
                repositorio = new DALMongoDB.GenericRepository<Lectura>();
            }
            else
            {
                repositorio = new DALApi.GenericRepository<Lectura>();
            }
            Console.WriteLine("Dame el numero de id del dispositivo: ");
            string id=Console.ReadLine();
            List<Lectura> lecturas = repositorio.Read.Where(l => l.IdDispositivo == id).ToList();
            Console.WriteLine($"Se encontraron {lecturas.Count} registros");
            foreach (var item in lecturas)
            {
                Console.WriteLine($"Fecha:{item.FechaHora.ToShortDateString()}\t temp:{item.Temperatura}\t humedad:{item.Humedad}");
            }
            Console.ReadLine();
        }

    }
}
