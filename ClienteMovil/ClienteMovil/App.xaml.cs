﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ClienteMovil
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            var n= new NavigationPage(new MainPage());
            n.BarBackgroundColor = Color.Pink;
            n.BarTextColor = Color.Black;
            MainPage = n;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
