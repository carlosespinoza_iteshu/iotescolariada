﻿using COMMON.Entidades;
using COMMON.Interfaces;
using DALMongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ClienteMovil
{
    public partial class MainPage : ContentPage
    {
        IGenericRepository<Dispositivo> dispositivoRepository;
        public MainPage()
        {
            InitializeComponent();
            try
            {
                dispositivoRepository = new GenericRepository<Dispositivo>();
                pkrDispositivos.ItemsSource = dispositivoRepository.Read.ToList();
            }
            catch (Exception)
            {
                DisplayAlert("Error", "Parece que no hay conexión con internet...","Ok");
                btnEntrar.IsEnabled = false;
            }
            
        }

        private void BtnEntrar_Clicked(object sender, EventArgs e)
        {
            Dispositivo d = pkrDispositivos.SelectedItem as Dispositivo;
            if (d != null)
            {
                if (d.Password == entPassword.Text) {
                    Navigation.PushAsync(new Views.Menu(d));
                }
                else
                {
                    DisplayAlert("Error", "Contraseña incorrecta", "Ok");
                }
            }
            else
            {
                DisplayAlert("Error", "Primero debes seleccionar un dispositivo...", "Ok");
            }
        }
    }
}
