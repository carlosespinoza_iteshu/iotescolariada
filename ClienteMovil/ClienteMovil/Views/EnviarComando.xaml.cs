﻿using COMMON.Entidades;
using COMMON.Interfaces;
using DALMongoDB;
using DALMqtt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClienteMovil.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EnviarComando : ContentPage
	{
        Dispositivo dispositivo;
        IGenericRepository<Dispositivo> repository;
        IGenericRepository<Comando> repositoryComandos;
        MqttService mqtt;
		public EnviarComando (Dispositivo dispositivo)
		{
			InitializeComponent ();
            repository = new GenericRepository<Dispositivo>();
            repositoryComandos = new GenericRepository<Comando>();
            pkrDispositivos.ItemsSource = repository.Read.ToList();
            this.dispositivo = dispositivo;
            mqtt = new MqttService("broker.hivemq.com", 1883, generarNombre());
            mqtt.MensajeRecibido += Mqtt_MensajeRecibido;
		}

        private void Mqtt_MensajeRecibido(object sender, string e)
        {
        }

        private string generarNombre()
        {
            return DateTime.Now.Ticks.ToString();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            Comando cmd = new Comando()
            {
                ComandoTexto = entComando.Text,
                Origen = "App Movil " + dispositivo.Nombre,
                IdDispositivo = (pkrDispositivos.SelectedItem as Dispositivo).Id
            };
            repositoryComandos.Create(cmd);
            mqtt.Publicar(dispositivo.Id, entComando.Text);
        }
    }
}