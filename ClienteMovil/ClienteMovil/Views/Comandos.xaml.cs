﻿using COMMON.Entidades;
using COMMON.Interfaces;
using DALMongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClienteMovil.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Comandos : ContentPage
	{
        IGenericRepository<Comando> repository;
		public Comandos (Dispositivo dispositivo)
		{
			InitializeComponent ();
            repository =new GenericRepository<Comando>();
            lstComandos.ItemsSource = repository.Query(c => c.IdDispositivo == dispositivo.Id);
            Title = "Comandos de " + dispositivo.Nombre;
		}

    
    }
}