﻿using COMMON.Entidades;
using COMMON.Interfaces;
using DALMongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClienteMovil.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Lecturas : ContentPage
	{
        Dispositivo dispositivo;
        IGenericRepository<Lectura> repository;
		public Lecturas (Dispositivo dispositivo)
		{
			InitializeComponent ();
            this.dispositivo = dispositivo;
            repository = new GenericRepository<Lectura>();
            Title = $"Lecturas de {dispositivo.Nombre}";
		}

        private void BtnBuscar_Clicked(object sender, EventArgs e)
        {
            lstLecturas.ItemsSource = repository.Query(d => d.IdDispositivo == dispositivo.Id && d.FechaHora > dtpInicio.Date && d.FechaHora < dtpFin.Date).OrderBy(l=>l.FechaHora);

        }
    }
}