﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClienteMovil.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Menu : ContentPage
	{
        Dispositivo dispositivo;
		public Menu (Dispositivo dispositivo)
		{
			InitializeComponent ();
            this.Title = dispositivo.Nombre;
            this.dispositivo = dispositivo;
		}

        private void BtnLecturas_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Views.Lecturas(dispositivo));
        }

        private void BtnComandos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Views.Comandos(dispositivo));
        }

        private void BtnEnviar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Views.EnviarComando(dispositivo));
        }

        private void BtnAcerca_Clicked(object sender, EventArgs e)
        {

        }
    }
}